package MsgModel._Base;

/**
 * Created by Administrator on 2016/12/12 0012.
 */
public class BasePageCountMsg extends BaseCountMsg {
    private String PageName;
    private boolean IsSumByPageId;
    private String PageId;
    private String Param;

    public BasePageCountMsg() {
    }

    public BasePageCountMsg(String userIp, String serverIp, String from, long timeStamp, long sendTimeStamp, String userId, String deviceType, String cityName, String pageName, boolean isSumByPageId, String pageId, String param) {
        super(userIp, serverIp, from, timeStamp, sendTimeStamp, userId, deviceType, cityName);
        PageName = pageName;
        IsSumByPageId = isSumByPageId;
        PageId = pageId;
        Param = param;
    }

    public String getPageName() {
        return PageName;
    }

    public void setPageName(String pageName) {
        PageName = pageName;
    }

    public Boolean getIsSumByPageId() {
        return IsSumByPageId;
    }

    public void getIsSumByPageId(Boolean sumByPageId) {
        IsSumByPageId = sumByPageId;
    }

    public String getPageId() {
        return PageId;
    }

    public void setPageId(String pageId) {
        PageId = pageId;
    }

    public String getParam() {
        return Param;
    }

    public void setParam(String param) {
        Param = param;
    }
}
