package MsgModel.Result;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/21 0021.
 */
public class LoginCountResult implements Serializable {



    private String LoginTime;
    private int Count;
    private Long CreateOrUpdateTime;

    public LoginCountResult() {
    }

    public LoginCountResult( String loginTime, int count, Long createOrUpdateTime) {
        LoginTime = loginTime;
        Count = count;
        CreateOrUpdateTime = createOrUpdateTime;
    }



    public String getLoginTime() {
        return LoginTime;
    }

    public void setLoginTime(String loginTime) {
        LoginTime = loginTime;
    }

    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }

    public Long getCreateOrUpdateTime() {
        return CreateOrUpdateTime;
    }

    public void setCreateOrUpdateTime(Long createOrUpdateTime) {
        CreateOrUpdateTime = createOrUpdateTime;
    }
}
