package MsgModel.UserBehavior;

import MsgModel._Base._BaseMsg;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/14 0014.
 */
public class UserBehaviorMsg extends _BaseMsg implements Serializable {
    public UserBehaviorMsg() {
    }

    public UserBehaviorMsg(String userIp, String serverIp, String from, long timeStamp, long sendTimeStamp, String userId, String behavior, String behaviorName, String behaviorPara, String behaviorPara1, String behaviorPara2, String behaviorPara3, String behaviorPara4, String userOtherPara) {
        super(userIp, serverIp, from, timeStamp, sendTimeStamp);
        UserId = userId;
        Behavior = behavior;
        BehaviorName = behaviorName;
        BehaviorPara = behaviorPara;
        BehaviorPara1 = behaviorPara1;
        BehaviorPara2 = behaviorPara2;
        BehaviorPara3 = behaviorPara3;
        BehaviorPara4 = behaviorPara4;
        UserOtherPara = userOtherPara;
    }

    private String UserId;
    /**
     * 行为类型： login,view,sub....
     */
    private String Behavior;
    /**
     * 行为内容： login:null,view:pageName,sub:BA...
     */
    private String BehaviorName;
    /**
     * 行为内容业务ID:pageId,sub:BAId...
     */
    private String BehaviorPara;
    private String BehaviorPara1;
    private String BehaviorPara2;
    private String BehaviorPara3;
    private String BehaviorPara4;
    private String UserOtherPara;
    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getBehavior() {
        return Behavior;
    }

    public void setBehavior(String behavior) {
        Behavior = behavior;
    }

    public String getBehaviorName() {
        return BehaviorName;
    }

    public void setBehaviorName(String behaviorName) {
        BehaviorName = behaviorName;
    }

    public String getBehaviorPara() {
        return BehaviorPara;
    }

    public void setBehaviorPara(String behaviorPara) {
        BehaviorPara = behaviorPara;
    }

    public String getBehaviorPara1() {
        return BehaviorPara1;
    }

    public void setBehaviorPara1(String behaviorPara1) {
        BehaviorPara1 = behaviorPara1;
    }

    public String getBehaviorPara2() {
        return BehaviorPara2;
    }

    public void setBehaviorPara2(String behaviorPara2) {
        BehaviorPara2 = behaviorPara2;
    }

    public String getBehaviorPara3() {
        return BehaviorPara3;
    }

    public void setBehaviorPara3(String behaviorPara3) {
        BehaviorPara3 = behaviorPara3;
    }

    public String getBehaviorPara4() {
        return BehaviorPara4;
    }

    public void setBehaviorPara4(String behaviorPara4) {
        BehaviorPara4 = behaviorPara4;
    }

    public String getUserOtherPara() {
        return UserOtherPara;
    }

    public void setUserOtherPara(String userOtherPara) {
        UserOtherPara = userOtherPara;
    }
}
