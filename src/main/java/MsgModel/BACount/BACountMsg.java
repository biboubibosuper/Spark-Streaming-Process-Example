package MsgModel.BACount;

import MsgModel._Base.BaseCountMsg;

/**
 * Created by Administrator on 2016/12/14 0014.
 */
public class BACountMsg extends BaseCountMsg {

    public BACountMsg() {
    }

    public BACountMsg(String userIp, String serverIp, String from, long timeStamp, long sendTimeStamp, String userId, String deviceType, String cityName, String BAId, Boolean addOrCancel) {
        super(userIp, serverIp, from, timeStamp, sendTimeStamp, userId, deviceType, cityName);
        this.BAId = BAId;
        AddOrCancel = addOrCancel;
    }

    private String BAId;
    private Boolean AddOrCancel;

    public String getBAId() {
        return BAId;
    }

    public void setBAId(String BAId) {
        this.BAId = BAId;
    }

    public Boolean getAddOrCancel() {
        return AddOrCancel;
    }

    public void setAddOrCancel(Boolean addOrCancel) {
        AddOrCancel = addOrCancel;
    }
}
