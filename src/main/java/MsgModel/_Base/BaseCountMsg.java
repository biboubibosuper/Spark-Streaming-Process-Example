package MsgModel._Base;

/**
 * Created by Administrator on 2016/12/12 0012.
 */
public class BaseCountMsg extends _BaseMsg {
    private String UserId;
    private String DeviceType;
    private String CityName;

    public BaseCountMsg() {
    }

    public BaseCountMsg(String userIp, String serverIp, String from, long timeStamp, long sendTimeStamp, String userId, String deviceType, String cityName) {
        super(userIp, serverIp, from, timeStamp, sendTimeStamp);
        UserId = userId;
        DeviceType = deviceType;
        CityName = cityName;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDeviceType() {
        return DeviceType;
    }

    public void setDeviceType(String deviceType) {
        DeviceType = deviceType;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

}
