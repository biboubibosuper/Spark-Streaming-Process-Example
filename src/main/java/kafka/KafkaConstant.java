package kafka;

/**
 * Created by hy on 2017/01/17 0017.
 */
public class KafkaConstant {
    public static final int TIMEOUT = 3000;
    public static final int BUFFERSIZE = 1024*1024;
    public static final String GROUPID= "Get-Offset-Group";
    public static final String Mongo_Pwd = "sa";
}
